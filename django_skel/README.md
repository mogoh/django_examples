A Simple Django Application Skeleton.

```
mkvirtualenv django_skel
workon django_skel
pip install -r requirements.txt
python3 manage.py migrate
python3 manage.py runserver 8080
```
