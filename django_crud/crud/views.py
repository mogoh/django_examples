from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.views.generic.detail import DetailView

from .models import Data
from .forms import DataForm


class Index(TemplateView):
    template_name = "crud/index.html"


class Create(CreateView):
    model = Data
    fields = '__all__'
    success_url = '../list'


class Read(DetailView):
    model = Data


class Delete(DeleteView):
    model = Data
    success_url = '../list'


class Update(UpdateView):
    model = Data
    # fields = '__all__'
    success_url = '../list'
    form_class = DataForm


class List(ListView):
    model = Data
