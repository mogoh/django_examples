from django import forms
from .models import Data


class DataForm(forms.ModelForm):
    class Meta:
        model = Data
        fields = '__all__'

    def save(self, commit=True):
        return super().save(commit)