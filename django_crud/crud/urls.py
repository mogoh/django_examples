from django.urls import path

from . import views

urlpatterns = [
    path('', views.Index.as_view(), name='index'),
    path('create', views.Create.as_view(), name='create'),
    path('read/<int:pk>', views.Read.as_view(), name='read'),
    path('update/<int:pk>', views.Update.as_view(), name='update'),
    path('delete/<int:pk>', views.Delete.as_view(), name='delete'),
    path('list', views.List.as_view(), name='list'),
]
