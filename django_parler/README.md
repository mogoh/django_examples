# Django + Parler

```
mkvirtualenv django_parler
workon django_parler
pip install -r requirements.txt

sudo -u postgres psql
CREATE ROLE admin WITH LOGIN PASSWORD 'password' CREATEDB SUPERUSER;
\q
sudo -u postgres createdb --template=template0 --owner=admin django-parler

python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```

Visit http://localhost:8000/admin and create an object.
Visit http://localhost:8000/detail/1 and edit the object.
