from django.forms import ModelForm, CharField

from .models import Example


class ExampleUpdateForm(ModelForm):
    a = CharField(max_length=100, label="A")
    b_en = CharField(max_length=100, label="B")

    class Meta:
        model = Example
        fields = [
            'a',
            'b_en',
        ]

    def save(self, commit=True):
        # Calling overridden save-method
        instance = super(ExampleUpdateForm, self).save(commit=commit)

        # Getting the cleaned data
        data = self.cleaned_data

        # We can not create e.g. translations without saving the base object first
        if commit:
            # Update user object
            instance.a = data['a']
            instance.save()

            # Create/Update translations
            if not instance.has_translation('en'):
                instance.create_translation('de')
            en = instance.get_translation('en')
            en.b = data['b_en']
            en.save()
            instance.save()
            instance.save_translations()
        return instance
