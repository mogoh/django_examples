from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('detail/<int:pk>', views.ParlerDetailView.as_view(), name='detail'),
    path('update/<int:pk>', views.ParlerUpdateView.as_view(), name='update'),
]