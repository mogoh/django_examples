from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import UpdateView
from django.urls import reverse_lazy

from .models import Example
from .forms import ExampleUpdateForm


class IndexView(TemplateView):
    template_name = "parler_example/index.html"


class ParlerDetailView(DetailView):
    model = Example
    def get_context_data(self, object, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = object
        context['pk'] = self.object.pk
        return context

class ParlerUpdateView(UpdateView):
    model = Example
    form_class = ExampleUpdateForm

    def get_initial(self):
        initial = super().get_initial()
        initial['a'] = self.object.a
        initial['b_en'] = self.object.get_translation('en').b

        return initial

    def get_success_url(self):
        return reverse_lazy('detail', kwargs={'pk': self.object.pk})
