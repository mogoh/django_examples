from django.contrib import admin
from parler.admin import TranslatableAdmin
from .models import Example

# Register your models here.

@admin.register(Example)
class ExampleAdmin(TranslatableAdmin):
    model = Example
