from django.db import models

from parler.models import TranslatableModel
from parler.models import TranslatedFields


class Example(TranslatableModel):
    a = models.CharField("A", max_length=100, blank=True, null=True)
    translations = TranslatedFields(
        b=models.CharField("B", max_length=100, blank=True, null=True),
    )
