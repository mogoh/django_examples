from django.apps import AppConfig


class ParlerExampleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'parler_example'
