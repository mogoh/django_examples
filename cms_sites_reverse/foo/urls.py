from django.urls import re_path

from .views import FooView

app_name = 'foo'
urlpatterns = [
    re_path(r'^$', FooView.as_view(), name='foo'),
]
