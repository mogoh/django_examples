from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


@apphook_pool.register  # register the application
class FooApphook(CMSApp):
    app_name = "foo"
    name = "foo"

    def get_urls(self, page=None, language=None, **kwargs):
        return ["foo.urls"]