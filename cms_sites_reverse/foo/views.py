from django.urls import reverse
from django.views.generic import TemplateView
from django.contrib.sites.models import Site


class FooView(TemplateView):
    template_name = 'foo.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["url"] = self.get_contextual_url()
        return context


    def get_absolute_url(self) -> str:
        self.request.site = Site.objects.get(pk=2)
        return reverse("foo:foo")
    
    def get_contextual_url(self) -> str:
        return self.get_absolute_url()
