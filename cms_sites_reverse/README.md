# Django + DjangoCMS Sites reverse Example

Create the virtual environment:

```bash
python -m venv .venv
```

Enter the virtual environment:

```bash
. .venv/bin/activate
```

Install requirements:

```bash
pip install -r requirements.txt
```

Migrate the database:

```bash
./manage.py migrate
```

Create a supersuser account:

```bash
./manage.py createsuperuser
```

Run the server:

```
./manage.py runserver
```

- Got to: http://localhost:8000/en/admin/sites/site/
- Log in.
- Create two Sites. (a.example.com and b.example.com)

Run both servers:

```bash
export DJANGO_SETTINGS_MODULE=cmsproject.settings_a; ./manage.py runserver localhost:8000
export DJANGO_SETTINGS_MODULE=cmsproject.settings_b; ./manage.py runserver localhost:8001
```

- Create a page on a.example.com named FooA with app Foo.
- Create a page on b.example.com named FooB with app Foo.

Visits the app page